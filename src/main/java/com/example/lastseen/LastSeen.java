/*
 * Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 */

package com.example.lastseen;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.Date;

public class LastSeen {
    
    public static String getLastSeenText(String dateTime) throws IllegalArgumentException {
        if (dateTime == null || dateTime.trim().isEmpty()) {
            throw new IllegalArgumentException("Datetime string is empty");
        }
        try {
            DateTime dateTimeInStringFormat = DateTime.parse(dateTime);
            return getLastSeenText(dateTimeInStringFormat);
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException(ex.getMessage() + ". Try yyyy-MM-ddTHH:mm:ss");
        }
    }
    
    public static String getLastSeenText(Date date) throws IllegalArgumentException {
        if (date == null) { throw new IllegalArgumentException("Date is null"); }
        return getLastSeenText(new DateTime(date));
    }
    
    public static String getLastSeenText(DateTime dateTime) throws IllegalArgumentException {
        DateTime now = DateTime.now();
        if (now.isBefore(dateTime)) { throw new IllegalArgumentException("DateTime is in future"); }
        String result = "Last seen now";
        Period period = new Period(dateTime, now);
        long years = period.getYears();
        if (years > 0) {
            return getLastSeenText(years, "year");
        }
        long months = period.getMonths();
        if (months > 0) {
            return getLastSeenText(months, "month");
        }
        long days = period.getDays();
        if (days > 0) {
            return getLastSeenText(days, "day");
        }
        long hours = period.getHours();
        if (hours > 0) {
            return getLastSeenText(hours, "hour");
        }
        long minutes = period.getMinutes();
        if (minutes > 0) {
            return getLastSeenText(minutes, "minute");
        }
        return result;
    }

    private static String getLastSeenText(long value, String timeName) {
        return "Last seen " + value + " " + timeName + (value > 1 ? "s" : "") + " ago";
    }
}
