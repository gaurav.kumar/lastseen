/*
 * Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 */

package com.example.lastseen;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class LastSeenTest {

    @Test
    public void testLastSeenForYear() {
        DateTime dateTime = DateTime.now().minusYears(2);
        Assert.assertEquals("Last seen 2 years ago", LastSeen.getLastSeenText(dateTime));
        dateTime = DateTime.now().minusYears(1);
        Assert.assertEquals("Last seen 1 year ago", LastSeen.getLastSeenText(dateTime));
    }

    @Test
    public void testLastSeenForMonth() {
        DateTime dateTime = DateTime.now().minusMonths(4);
        Assert.assertEquals("Last seen 4 months ago", LastSeen.getLastSeenText(dateTime));
        dateTime = DateTime.now().minusMonths(1);
        Assert.assertEquals("Last seen 1 month ago", LastSeen.getLastSeenText(dateTime));
    }

    @Test
    public void testLastSeenForDay() {
        DateTime dateTime = DateTime.now().minusDays(2);
        Assert.assertEquals("Last seen 2 days ago", LastSeen.getLastSeenText(dateTime));
        dateTime = DateTime.now().minusDays(1);
        Assert.assertEquals("Last seen 1 day ago", LastSeen.getLastSeenText(dateTime));
    }

    @Test
    public void testLastSeenForHour() {
        DateTime dateTime = DateTime.now().minusHours(2);
        Assert.assertEquals("Last seen 2 hours ago", LastSeen.getLastSeenText(dateTime));
        dateTime = DateTime.now().minusHours(1);
        Assert.assertEquals("Last seen 1 hour ago", LastSeen.getLastSeenText(dateTime));
    }

    @Test
    public void testLastSeenForMinute() {
        DateTime dateTime = DateTime.now().minusMinutes(2);
        Assert.assertEquals("Last seen 2 minutes ago", LastSeen.getLastSeenText(dateTime));
        dateTime = DateTime.now().minusMinutes(1);
        Assert.assertEquals("Last seen 1 minute ago", LastSeen.getLastSeenText(dateTime));
    }

    @Test
    public void testLastSeenForNow() {
        DateTime dateTime = DateTime.now().minusSeconds(5);
        Assert.assertEquals("Last seen now", LastSeen.getLastSeenText(dateTime));
    }
    
    @Test
    public void testLastSeenForArbitrary() {
        DateTime dateTime = DateTime.now().minusYears(1).minusDays(2).minusMinutes(4);
        Assert.assertEquals("Last seen 1 year ago", LastSeen.getLastSeenText(dateTime));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLastSeenForException() {
        DateTime dateTime = DateTime.now().plusSeconds(5);
        LastSeen.getLastSeenText(dateTime);
    }

    @Test
    public void testLastSeenForJavaDate() {
        Date date = DateTime.now().minusHours(2).toDate();
        Assert.assertEquals("Last seen 2 hours ago", LastSeen.getLastSeenText(date));
    }

    @Test
    public void testLastSeenForString() {
        DateTime dateTime = DateTime.now().minusMinutes(2);
        String sample = String.format(
                "%d-%d-%dT%d:%d:%d",
                dateTime.getYear(),
                dateTime.getMonthOfYear(),
                dateTime.getDayOfMonth(),
                dateTime.getHourOfDay(),
                dateTime.getMinuteOfHour(),
                dateTime.getSecondOfMinute()
                );
        Assert.assertEquals("Last seen 2 minutes ago", LastSeen.getLastSeenText(sample));
    }
}